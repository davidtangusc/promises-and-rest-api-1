var mysql = require('mysql');
var genre = require('./genre');
var artist = require('./artist');

module.exports = {
  get(id) {
    var connection = mysql.createConnection({
      host     : 'itp460.usc.edu',
      user     : 'student',
      password : 'ttrojan',
      database : 'music'
    });

    connection.connect();
    return new Promise(function(resolve, reject) {
      connection.query('SELECT * FROM songs WHERE id = ?', [ id ], function(error, songs) {
        if (error) {
          reject();
        }

        if (songs.length >= 1) {
          var song = songs[0];
          Promise.all([ artist.get(song.artist_id), genre.get(song.genre_id) ]).then(function(results) {
            song.artist = results[0];
            song.genre = results[1];
            resolve(song);
          });
        } else {
          reject();
        }
      });
    });
  }
};
