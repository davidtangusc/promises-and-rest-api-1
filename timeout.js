function timeout(milliseconds) {
  return new Promise(function(resolve, reject) {
    setTimeout(function() {
      // resolve();
      reject();
    }, milliseconds);
  });
}

console.log(new Date());
timeout(1000).then(function() {
  console.log(new Date());
  timeout(1000).then(function() {
    console.log(new Date());
  });
}, function() {
  console.log('rejected');
});
