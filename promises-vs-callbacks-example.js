// Callback style
db.query('SELECT * FROM songs', function(error, results) {

});

// Promise style
// promises can be return values
// promises are chainable
var promise = db.query('SELECT * FROM songs');
promise.then(function(results) {

}).then(function() {

});

// Can separate out error handling with promises
db.query('SELECT * FROM songs WHERE id = ?').then(function() {
  console.log('success');
}, function() {
  console.log('error');
});

db.query('SELECT * FROM songs', function(error, results) {
  if (error) {
    console.log('error');
    return;
  }

  console.log('success');
});

// example with multiple promises
app.post('/songs', function (request, response) {
  Auth.user()
      .then(getUserPermissions)
      .then(insertSong, noPermissionsToCreateSong)
      .then(renderSongJSON, renderErrorJSON);
});
