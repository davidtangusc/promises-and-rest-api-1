var express = require('express');
var app = express();
var song = require('./song');

app.get('/songs/:id', function (request, response) {
  song.get(request.params.id).then(function(song) {
    response.json(song);
  }, function() {
    response.status(404).json({
      error: 'song not found'
    });
  });
});

app.listen(8000);
