* Promises are another way of managing asynchronous code.
* How do they differ from callbacks?
  * they have a return value
  * they are chainable
  * you can separate out error handling
* Promise states = pending, resolved, rejected
* ES6 Promise constructor
* Promise libraries
  * q
  * RSVP
  * Bluebird
